import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'cp8-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css'],
})
export class Cp8PageComponent {
  modal = false;

  openModal() {
    this.modal = true;
  }

  @HostListener('window:keyup.ctrl.x')
  @HostListener('window:keyup.control.x')
  closeModal() {
    this.modal = false;
  }
}
