import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Priorities } from '../enums/priorities';

@Component({
  selector: 'cp8-base-priority-select',
  templateUrl: './base-priority-select.component.html',
  styleUrls: ['./base-priority-select.component.scss'],
})
export class Cp8PrioritySelectComponent {
  @Input() selected: Priorities = Priorities.MEDIUM;
  @Output() selectedChange = new EventEmitter<Priorities>();

  priorities = Priorities;
}
