import { Component, Input } from '@angular/core';
import { Priorities } from '../enums/priorities';

@Component({
  selector: 'cp8-base-priority',
  templateUrl: './base-priority.component.html',
  styleUrls: ['./base-priority.component.scss'],
})
export class Cp8PriorityComponent {
  priorities = Priorities;
  @Input() level = Priorities.MEDIUM;
}
