export enum Priorities {
  LOW = 'Priorities:LOW',
  MEDIUM = 'Priorities:MEDIUM',
  HIGH = 'Priorities:HIGH',
}
