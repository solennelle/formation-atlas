import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Cp8ButtonComponent } from './base-button/base-button.component';
import { FormsModule } from '@angular/forms';
import { Cp8HeaderComponent } from './base-header/base-header.component';
import { Cp8InputComponent } from './base-input/base-input.component';
import { Cp8ModalComponent } from './base-modal/base-modal.component';
import { Cp8PriorityComponent } from './base-priority/base-priority.component';
import { Cp8PrioritySelectComponent } from './base-priority-select/base-priority-select.component';
import { Cp8PageComponent } from './page/page.component';
@NgModule({
  declarations: [
    Cp8ButtonComponent,
    Cp8HeaderComponent,
    Cp8InputComponent,
    Cp8ModalComponent,
    Cp8PriorityComponent,
    Cp8PrioritySelectComponent,
    Cp8PageComponent,
  ],
  imports: [CommonModule, FormsModule],
  exports: [
    Cp8ButtonComponent,
    Cp8HeaderComponent,
    Cp8InputComponent,
    Cp8ModalComponent,
    Cp8PriorityComponent,
    Cp8PrioritySelectComponent,
    Cp8PageComponent,
  ],
})
export class Cp8Module {}
