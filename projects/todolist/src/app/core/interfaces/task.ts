import { Priorities } from '../enums/priorities';
import { TaskDate } from './task-date';

export interface Task {
  id: number;
  title: string;
  description: string;
  date: TaskDate;
  priority: Priorities;
}
