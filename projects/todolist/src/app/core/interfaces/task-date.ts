export interface TaskDate {
  creation: TimeStamp;
  completion?: TimeStamp;
  due: TimeStamp;
}

type TimeStamp = number;
