import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, of } from 'rxjs';
import { Task } from './interfaces/task';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Priorities } from './enums/priorities';

@Injectable({
  providedIn: 'root',
})
export class TaskManagerService {
  constructor(private http: HttpClient) {
    this.load();
  }

  data$ = new BehaviorSubject<Task[]>([]);

  /** Loads the existing tasks from the api endpoint and converts it bc of the enum */
  load(): any {
    return this.http
      .get<TaskFromServer[]>(environment.API)
      .pipe(map(this.toAppFormat))
      .subscribe((data) => this.data$.next(data));
  }

  /** Creates a task */
  toAppFormat(data: TaskFromServer[]): Task[] {
    return data.map((task) => {
      const mapTask = { ...task, priority: Priorities.MEDIUM };
      switch (task.priority) {
        case 1:
          mapTask.priority = Priorities.HIGH;
          break;
        case 2:
          mapTask.priority = Priorities.MEDIUM;
          break;
        case 3:
          mapTask.priority = Priorities.LOW;
          break;
        default:
          break;
      }
      return mapTask;
    });
  }

  toServerFormat(data: Task): TaskFromServer {
    let serverTask: TaskFromServer = { ...data, priority: 1 };
    switch (data.priority) {
      case Priorities.LOW:
        serverTask.priority = 1;
        break;
      case Priorities.MEDIUM:
        serverTask.priority = 2;
        break;
      case Priorities.HIGH:
        serverTask.priority = 3;
        break;
      default:
        break;
    }
    return serverTask;
  }

  /** Creates a task */
  createTask(data: Omit<Task, 'id'>): Observable<boolean> {
    const obs$ = new BehaviorSubject(true);
    this.http.post<any>(environment.API, data).subscribe((resData) => {
      // on ajoute la tâche créée à la liste observable
      this.data$.next([...this.data$.value, resData]);
      obs$.next(true);
    });
    return obs$;
  }

  /** Completes a task */
  completeTask(task: Task): Task {
    if (task.date.completion) {
      delete task.date.completion;
      return task;
    }
    task.date.completion = Date.now();
    return task;
  }
}

interface TaskFromServer extends Omit<Task, 'priority'> {
  priority: ServerPriority;
}

type ServerPriority = 1 | 2 | 3;
