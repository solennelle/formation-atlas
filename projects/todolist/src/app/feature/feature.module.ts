import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureTaskCreationComponent } from './feature-task-creation/feature-task-creation.component';
import { FeatureTaskListComponent } from './feature-task-list/feature-task-list.component';
import { FeatureTaskFilterComponent } from './feature-task-filter/feature-task-filter.component';
import { FeatureTaskStatComponent } from './feature-task-stat/feature-task-stat.component';
import { FeatureTaskListItemComponent } from './feature-task-list-item/feature-task-list-item.component';
import { Cp8Module } from 'projects/cp8/src/public-api';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    FeatureTaskCreationComponent,
    FeatureTaskListComponent,
    FeatureTaskFilterComponent,
    FeatureTaskStatComponent,
    FeatureTaskListItemComponent,
  ],
  imports: [CommonModule, Cp8Module, FormsModule],
  exports: [
    FeatureTaskCreationComponent,
    FeatureTaskListComponent,
    FeatureTaskFilterComponent,
    FeatureTaskStatComponent,
  ],
})
export class FeatureModule {}
