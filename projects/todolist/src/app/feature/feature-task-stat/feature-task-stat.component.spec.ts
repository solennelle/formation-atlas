import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureTaskStatComponent } from './feature-task-stat.component';

describe('FeatureTaskStatComponent', () => {
  let component: FeatureTaskStatComponent;
  let fixture: ComponentFixture<FeatureTaskStatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeatureTaskStatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureTaskStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
