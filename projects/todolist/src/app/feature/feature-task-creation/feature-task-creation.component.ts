import { Component, Output, EventEmitter } from '@angular/core';
import { Priorities } from '../../core/enums/priorities';
import { Task } from '../../core/interfaces/task';
import { TaskManagerService } from '../../core/task-manager.service';

@Component({
  selector: 'app-feature-task-creation',
  templateUrl: './feature-task-creation.component.html',
  styleUrls: ['./feature-task-creation.component.scss'],
})
export class FeatureTaskCreationComponent {
  constructor(public taskService: TaskManagerService) {}

  @Output() taskCreated = new EventEmitter<boolean>();
  priorities = Priorities;

  task: Omit<Task, 'id'> = {
    date: {
      completion: 0,
      creation: Date.now(),
      due: Date.now(),
    },
    description: '',
    title: '',
    priority: Priorities.MEDIUM,
  };

  submit() {
    this.taskService.createTask(this.task);
    console.log('task complete');
    this.taskCreated.emit(true);
  }
}
