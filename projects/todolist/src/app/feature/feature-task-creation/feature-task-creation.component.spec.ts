import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureTaskCreationComponent } from './feature-task-creation.component';

describe('FeatureTaskCreationComponent', () => {
  let component: FeatureTaskCreationComponent;
  let fixture: ComponentFixture<FeatureTaskCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeatureTaskCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureTaskCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
