import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Priorities } from '../../core/enums/priorities';
import { Task } from '../../core/interfaces/task';

@Component({
  selector: 'app-feature-task-list-item',
  templateUrl: './feature-task-list-item.component.html',
  styleUrls: ['./feature-task-list-item.component.scss'],
})
export class FeatureTaskListItemComponent {
  @Input() task: Task = {
    id: 1,
    date: {
      creation: Date.now(),
      due: Date.now(),
    },
    description: 'Test description',
    title: 'Test titre',
    priority: Priorities.HIGH,
  };

  @Output() completeTask = new EventEmitter<Task>();
}
