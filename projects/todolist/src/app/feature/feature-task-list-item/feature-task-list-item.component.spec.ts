import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureTaskListItemComponent } from './feature-task-list-item.component';

describe('FeatureTaskListItemComponent', () => {
  let component: FeatureTaskListItemComponent;
  let fixture: ComponentFixture<FeatureTaskListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeatureTaskListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureTaskListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
