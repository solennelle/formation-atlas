import { Component } from '@angular/core';
import { Task } from '../../core/interfaces/task';
import { TaskManagerService } from '../../core/task-manager.service';

@Component({
  selector: 'app-feature-task-list',
  templateUrl: './feature-task-list.component.html',
  styleUrls: ['./feature-task-list.component.scss'],
})
export class FeatureTaskListComponent {
  constructor(public tm: TaskManagerService) {}

  tasks: Task[] = [];

  handleComplete(task: Task): void {
    this.tm.completeTask(task);
  }
}
