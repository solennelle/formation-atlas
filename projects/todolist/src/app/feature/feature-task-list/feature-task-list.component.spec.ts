import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureTaskListComponent } from './feature-task-list.component';

describe('FeatureTaskListComponent', () => {
  let component: FeatureTaskListComponent;
  let fixture: ComponentFixture<FeatureTaskListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeatureTaskListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureTaskListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
