import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureTaskFilterComponent } from './feature-task-filter.component';

describe('FeatureTaskFilterComponent', () => {
  let component: FeatureTaskFilterComponent;
  let fixture: ComponentFixture<FeatureTaskFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeatureTaskFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureTaskFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
