import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureModule } from '../feature/feature.module';
import { ViewHomeComponent } from './view-home/view-home.component';
import { Cp8Module } from 'projects/cp8/src/public-api';

@NgModule({
  declarations: [ViewHomeComponent],
  imports: [CommonModule, FeatureModule, Cp8Module],
  exports: [ViewHomeComponent],
})
export class ViewModule {}
