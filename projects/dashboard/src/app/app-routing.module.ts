import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageDashComponent } from './page/page-dash/page-dash.component';
import { PageHomeComponent } from './page/page-home/page-home.component';

const routes: Routes = [
  {
    path: '',
    component: PageHomeComponent,
  },
  { path: 'dash', component: PageDashComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
