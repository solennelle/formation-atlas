import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHomeComponent } from './page-home/page-home.component';
import { PageDashComponent } from './page-dash/page-dash.component';
import { Cp8Module } from 'projects/cp8/src/public-api';

@NgModule({
  declarations: [PageHomeComponent, PageDashComponent],
  imports: [CommonModule, Cp8Module],
  exports: [PageHomeComponent, PageDashComponent],
})
export class PageModule {}
