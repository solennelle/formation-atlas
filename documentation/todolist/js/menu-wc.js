'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">atlas documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Saisissez un texte"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Démarrage</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Vue d&#x27;ensemble
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dépendances
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Propriétés
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-2bf5affb55d6696eaec8a429bc1f2a49bba2187abc930fdfb3b67db543dd090ec9f70609e97ed4cbff1fd25d0e2727e01e512622fdaed8c3df777c8f24a8c814"' : 'data-target="#xs-components-links-module-AppModule-2bf5affb55d6696eaec8a429bc1f2a49bba2187abc930fdfb3b67db543dd090ec9f70609e97ed4cbff1fd25d0e2727e01e512622fdaed8c3df777c8f24a8c814"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Composants</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-2bf5affb55d6696eaec8a429bc1f2a49bba2187abc930fdfb3b67db543dd090ec9f70609e97ed4cbff1fd25d0e2727e01e512622fdaed8c3df777c8f24a8c814"' :
                                            'id="xs-components-links-module-AppModule-2bf5affb55d6696eaec8a429bc1f2a49bba2187abc930fdfb3b67db543dd090ec9f70609e97ed4cbff1fd25d0e2727e01e512622fdaed8c3df777c8f24a8c814"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/BaseModule.html" data-type="entity-link" >BaseModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-BaseModule-77e8a70999a59f7743b13ab9fdfa920823e3a662823e2f52ce897d943031611e17650d7a0ec751b66641e562f53e6e80a7ce1a5c50f233e120f386f9804e113d"' : 'data-target="#xs-components-links-module-BaseModule-77e8a70999a59f7743b13ab9fdfa920823e3a662823e2f52ce897d943031611e17650d7a0ec751b66641e562f53e6e80a7ce1a5c50f233e120f386f9804e113d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Composants</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-BaseModule-77e8a70999a59f7743b13ab9fdfa920823e3a662823e2f52ce897d943031611e17650d7a0ec751b66641e562f53e6e80a7ce1a5c50f233e120f386f9804e113d"' :
                                            'id="xs-components-links-module-BaseModule-77e8a70999a59f7743b13ab9fdfa920823e3a662823e2f52ce897d943031611e17650d7a0ec751b66641e562f53e6e80a7ce1a5c50f233e120f386f9804e113d"' }>
                                            <li class="link">
                                                <a href="components/BaseButtonComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BaseButtonComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BaseHeaderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BaseHeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BaseInputComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BaseInputComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BaseModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BaseModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BasePriorityComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BasePriorityComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BasePrioritySelectComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BasePrioritySelectComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link" >CoreModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/FeatureModule.html" data-type="entity-link" >FeatureModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-FeatureModule-44040f9ce82ec1ab3243e53945ee69159cfb9e22e842879f46acf3e8eae835114260b67a8cd5153955e2762c6b92519f69abd9ebe122635f1d5de14fdb2fe9d1"' : 'data-target="#xs-components-links-module-FeatureModule-44040f9ce82ec1ab3243e53945ee69159cfb9e22e842879f46acf3e8eae835114260b67a8cd5153955e2762c6b92519f69abd9ebe122635f1d5de14fdb2fe9d1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Composants</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FeatureModule-44040f9ce82ec1ab3243e53945ee69159cfb9e22e842879f46acf3e8eae835114260b67a8cd5153955e2762c6b92519f69abd9ebe122635f1d5de14fdb2fe9d1"' :
                                            'id="xs-components-links-module-FeatureModule-44040f9ce82ec1ab3243e53945ee69159cfb9e22e842879f46acf3e8eae835114260b67a8cd5153955e2762c6b92519f69abd9ebe122635f1d5de14fdb2fe9d1"' }>
                                            <li class="link">
                                                <a href="components/FeatureTaskCreationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FeatureTaskCreationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FeatureTaskFilterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FeatureTaskFilterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FeatureTaskListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FeatureTaskListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FeatureTaskListItemComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FeatureTaskListItemComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FeatureTaskStatComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FeatureTaskStatComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ViewModule.html" data-type="entity-link" >ViewModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ViewModule-8e3c2c370dd6d004d861638add6edcdf70a88e5a97dd29e372986252686feb075bd46b817f8234fb97f795b850c71edf447478686ab4286eb697880970546ba8"' : 'data-target="#xs-components-links-module-ViewModule-8e3c2c370dd6d004d861638add6edcdf70a88e5a97dd29e372986252686feb075bd46b817f8234fb97f795b850c71edf447478686ab4286eb697880970546ba8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Composants</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ViewModule-8e3c2c370dd6d004d861638add6edcdf70a88e5a97dd29e372986252686feb075bd46b817f8234fb97f795b850c71edf447478686ab4286eb697880970546ba8"' :
                                            'id="xs-components-links-module-ViewModule-8e3c2c370dd6d004d861638add6edcdf70a88e5a97dd29e372986252686feb075bd46b817f8234fb97f795b850c71edf447478686ab4286eb697880970546ba8"' }>
                                            <li class="link">
                                                <a href="components/ViewHomeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ViewHomeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Divers</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Couverture de documentation</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation générée avec <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});